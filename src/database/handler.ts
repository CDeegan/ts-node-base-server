import mongoose from 'mongoose';
import logger from '../logger/logger';
import { MongoMemoryServer } from 'mongodb-memory-server';

const mongod = new MongoMemoryServer({
	instance: {
		port: parseInt(process.env.DB_PORT as string, 10) || 27017,
		storageEngine: 'wiredTiger'
	}
});

/**
 * Connect to the in-memory database.
 */

export async function connect() {
	const uri = await mongod.getUri();
	const mongooseOpts = {
		useNewUrlParser: true,
		useCreateIndex: true,
		useFindAndModify: false,
		useUnifiedTopology: true
	};

	try {
		await mongoose.connect(uri, mongooseOpts);
		mongoose.connection.on('connected', () => logger.info(`MongoDB connection to ${uri} created`));
		mongoose.connection.on('disconnected', () => logger.info('MongoDB disconnected'));
		mongoose.connection.on('error', err => logger.error(err));
	} catch (e) {
		throw new Error(e);
	}
}

/**
 * Drop database, closes the connection and stop mongod.
 */
export async function close() {
	await mongoose.connection.dropDatabase();
	await mongoose.connection.close();
	await mongod.stop();
}

/**
 * Remove all the data across all db collections.
 */
export async function clear() {
	const collections = mongoose.connection.collections;

	for (const key of Object.keys(collections)) {
		const collection = collections[key];
		await collection.deleteMany({});
	}
}
