import cors from 'cors';
import dotenv from 'dotenv-flow';
import express from 'express';
import fs from 'fs';
import helmet from 'helmet';
import http from 'http';
import https from 'https';
import morgan from 'morgan';
import * as dbHandler from './database/handler';
import logger from './logger/logger';
import authenticator from './middleware/authentication';
import * as errorHandler from './middleware/error-handler';
import apiRouter from './routes/api';
dotenv.config();

try {
	dbHandler.connect();
} catch (e) {
	logger.error(e);
}

const app = express();

app.use(helmet());
app.use(cors());

app.use(express.json());
app.use(morgan('tiny', { stream: logger.getStream()}));
app.use('/api', authenticator, apiRouter);
app.all('*', errorHandler.notFound);
app.use(errorHandler.handleError);

const httpServer = http.createServer(app);
httpServer.on('error', e => logger.warn(e));

httpServer.listen(process.env.PORT || 80, () => {
	logger.info(`HTTP server listening on port ${process.env.PORT}`);
});

try {
	if (!process.env.SSL_CERT_PATH) logger.warn(`Environment is missing a SSL_CERT_PATH value`);
	else if (!process.env.SSL_KEY_PATH) logger.warn(`Environment is missing a SSL_KEY_PATH value`);
	else {
		const certificate = fs.readFileSync(process.env.SSL_CERT_PATH, 'utf8');
		const privateKey = fs.readFileSync(process.env.SSL_KEY_PATH, 'utf8');
		const credentials = { key: privateKey, cert: certificate };

		const httpsServer = https.createServer(credentials, app);
		httpsServer.on('error', e => logger.warn(e));

		httpsServer.listen(process.env.PORT_HTTPS || 443, () => {
			logger.info(`HTTPS server listening on port ${process.env.PORT_HTTPS}`);
		});
	}
} catch (e) {
	logger.warn(`SSL credentials not found, server could not be created`);
}
