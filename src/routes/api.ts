import express from 'express';
import * as sampleController from '../controllers/sample-controller';

const router = express.Router();

router.route('/sample/:id')
	.get(sampleController.getSample)
	.put(sampleController.updateSample)
	.delete(sampleController.deleteSample);

router.route('/sample/')
	.get(sampleController.getSamples)
	.post(sampleController.createSample)
	.delete(sampleController.deleteSamples);

export default router;
