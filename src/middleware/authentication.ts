import boom from 'boom';

const ipWhitelist = ['::1', process.env.ADMIN_IP_ADDRESS as string];

function getRequestIp(req: any): string {
	const ipAddress = (req.headers['x-forwarded-for'] || req.connection.remoteAddress || '').split(',')[0].trim();
	return parseIpAsV4(ipAddress);
}

function parseIpAsV4(ipAddress: string): string {
	const ipV6Prefix = '::ffff:';
	return ipAddress.indexOf(ipV6Prefix) === 0 ? ipAddress.substring(ipV6Prefix.length) : ipAddress;
}

function isPrivateIP(ipAddress: string): boolean {
	const parts = ipAddress.split('.');
	return parts[0] === '10' ||
		(parts[0] === '172' && (parseInt(parts[1], 10) >= 16 && parseInt(parts[1], 10) <= 31)) ||
		(parts[0] === '192' && parts[1] === '168');
}


export default function authenticate(req: any, res: any, next: any) {
	const ipAddress = getRequestIp(req);
	if (isPrivateIP(ipAddress)) {
		return next();
	} else if (ipWhitelist.indexOf(ipAddress) !== -1) {
		return next();
	} else {
		return next(boom.forbidden());
	}
}
