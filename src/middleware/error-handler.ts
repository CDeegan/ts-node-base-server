import * as Boom from 'boom';
import boom from 'boom';
import logger from '../logger/logger';

export function notFound(req: any, res: any, next: any) {
	return next(boom.notFound('Endpoint does not exist'));
}

export function handleError(err: Boom, req: any, res: any, next: any) {
	if (err.isServer) logger.error(err);
	return res.status(err.output.statusCode).json(err.output.payload);
}
